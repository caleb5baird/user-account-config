#!/bin/bash

function link_dotfiles () {
	set -e # Stop execution if I hit an error

	CONFIG_DIRECTORY="$HOME/.user-account-config"
	SOURCE_LOCATION="$CONFIG_DIRECTORY/dotfiles"
	TARGET_LOCATION="$HOME"

	# ------------------------------------------------ Install oh-my-zsh -----------------------------------------------
	echo "Installing oh-my-zsh..."

	"$CONFIG_DIRECTORY/tools/install-oh-my-zsh.sh"

	# remove the custom dir in .oh-my-zsh so that we can link to our custom dir
	rm -rf "$HOME/.oh-my-zsh/custom"

	# link our custom dir to .oh-my-zsh
	ln -sf "$CONFIG_DIRECTORY/custom" "$HOME/.oh-my-zsh"
	echo "Setting ZSH as default shell..."
	chsh -s /bin/zsh

	# populate git submodules in the custom dir
	local CWD
	CWD="$(pwd)"
	cd "$HOME/.oh-my-zsh/custom"
	git submodule init
	git submodule update
	cd "$CWD"

	# -------------------------------------------- Install my custom dofiles -------------------------------------------
	find "$SOURCE_LOCATION" -mindepth 1 -maxdepth 1 | cut -sd / -f 6- | while read -r file; do
		ln -sf "$SOURCE_LOCATION/$file" "$TARGET_LOCATION/.$file"
	done

	# ------------------------------------------------- Link nvim config -----------------------------------------------
	[ ! -d "$HOME/.config" ] && mkdir "$HOME/.config"
	ln -s "$CONFIG_DIRECTORY/nvim" "$HOME/.config/nvim"
	ln -s "$CONFIG_DIRECTORY/nvim/init.vim" "$HOME/.nvimrc"

	# ---------------------------------------------------- Link ~/bin --------------------------------------------------
	ln -sf "$CONFIG_DIRECTORY/bin" "$HOME"

	# -------------------------------------------- Create shortcuts directory ------------------------------------------
	# create_shortcuts

	# ---------------------------------------------------- Reload zsh --------------------------------------------------
	exec zsh -ls
	echo "Environment is setup"
}

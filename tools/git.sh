#!/bin/bash

SWD=$(pwd) # cash the staring working directory
cd "${BASH_SOURCE%/*}" || exit

. colors.sh

format green "Git global config ---------------------------------------------------------------------------------------"
git config --global user.name "Caleb Baird"
git config --global user.email "caleb5baird@gmail.com"

trap 'cd $SWD' EXIT

# vim:tabstop=4:shiftwidth=4

#!/bin/bash

SWD=$(pwd) # cash the starting working directory

#load in extra functions
cd "${BASH_SOURCE%/*}" || exit
. tools/functions.sh
. tools/colors.sh

cwd=$(pwd)


# -------------------------- Ask for the administrator password upfront -------------------------- #
format green "Start by getting permission--------------------------------------------------------------------"
echo "Please provide your administrator password..."
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.osx` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Create SSH key
if [[ ! -e "$HOME/.ssh/id_rsa" ]]; then
	format green "Creating an SSH key for you--------------------------------------------------------------------"
	ssh-keygen -t rsa
fi

# OS specific things ----------------------------- #
cd "$cwd" || exit
case "$OSTYPE" in
	darwin* ) . tools/osx.sh     ;;
	linux* )  . tools/linux.sh   ;;
	* )       format red "------------------------- !!! Don't know how to setup this distro !!! -------------------------";;
esac

format green "Setting up for development---------------------------------------------------------------------"
# GIT
cd "$cwd" && . tools/git.sh || exit

# NODE Maybe add this later
# cd "$cwd" && . tools/node.sh || exit 

sudo -H pip  install setuptools
sudo -H pip  install --upgrade neovim
sudo -H pip2 install --upgrade neovim
sudo -H pip3 install --upgrade neovim


format green "Linking dot files------------------------------------------------------------------------------"
link_dotfiles

trap 'cd $SWD' EXIT

# vim:tabstop=4:shiftwidth=4
